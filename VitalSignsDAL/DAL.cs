﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;
using PHEI.DTO;
using Db4objects.Db4o;

namespace VitalSignsDAL
{
    /// <summary>
    /// The Data Access Layer implemention for the DB40 Database.
    /// This is a very crude and simple mechanism for persistency, but
    /// it does hold both security, transcation and other relevant factors.
    /// You may modify or replace this layer as you wish ....
    /// </summary>
    public sealed class DAL
    {
        //Singleton utils
        static DAL instance = null;
        static readonly object padlock = new object();

        //Private modifiers for the datastore
        private string dataStoreFileName;
        private IObjectContainer db;

        /// <summary>
        /// Default empty constructor
        /// </summary>
        DAL () {
        }
            
        /// <summary>
        /// Use this to get a handle to the singleton DAL facade object
        /// </summary>
        /// <param name="dataStoreFileName">the path to the datastore</param>
        /// <returns>a DAL facade singleton object</returns>
        public static DAL Instance(string dataStoreFileName)
        {
                lock (padlock) //This assures no concurrency problems
                {
                    if (instance==null) { //if instance is null - then make a new one

                        instance = new DAL();
                        instance.dataStoreFileName = dataStoreFileName;
                        instance.db = Db4oFactory.OpenFile(dataStoreFileName);

                    } else if (instance.dataStoreFileName != dataStoreFileName) throw new Exception ("Wrong path provided");
                    
                    return instance;
                }
            }


        /// <summary>
        /// Destructor assures that the database is closed.
        /// </summary>
        ~DAL()
        {

            CloseDatabase();

        }

        /// <summary>
        /// Closes the datastore connection
        /// </summary>
        public void CloseDatabase()
        {
            try
            {
                db.Close();

            }
            catch
            {
                //TODO: consider logging errors here or reacting to it
            }
        }

        /// <summary>
        /// This will assign an ID to a given object
        /// </summary>
        /// <param name="obj">the dto object to assign an ID to </param>
        private void HandleID(AbstractDTO obj)
        {
            obj.ID = AssignID();
        }

        /// <summary>
        /// This will create a DTO object
        /// </summary>
        /// <param name="obj"></param>
        public void CreateDTO(AbstractDTO obj)
        {
            HandleID(obj);
            db.Store(obj);
        }


        /// <summary>
        /// Will retrieve a list of all vital signs on a current patient
        /// </summary>
        /// <param name="patient"></param>
        /// <returns>A list of vital signs belonging to the patient</returns>
        public IList<VitalSign> RetrieveAllVitalSignsOnPatient(Patient patient)
        {
            //Perform a query - using the delegate approach. All vital signs having this patient ID.
            IList<VitalSign> results = db.Query<VitalSign>(
                    delegate(VitalSign vs) 
                    {
                        return vs.PatientID == patient.ID; 
                    }
                );

            return results;
        }

        /// <summary>
        /// Will retrieve a list of patients belonging to a healthcare professional
        /// </summary>
        /// <param name="patient"></param>
        /// <returns>A list of vital signs belonging to the patient</returns>
        public IList<Patient> RetrieveAllPatientsOnHealthcareUser(HealthCareUser huser)
        {
            //Perform a query - using the delegate approach. All vital signs having this patient ID.
            IList<Patient> results = db.Query<Patient>(
                    delegate(Patient vs)
                    {
                        return vs.HealthCareUserID == huser.ID;
                    }
                );

            return results;
        }

        /// <summary>
        /// Will retrieve a list of patients belonging to a healthcare professional
        /// </summary>
        /// <param name="patient"></param>
        /// <returns>A list of vital signs belonging to the patient</returns>
        public IList<Patient> RetrieveAllPatientsOnHealthcareUser(long huserID)
        {
            //Perform a query - using the delegate approach. All vital signs having this patient ID.
            IList<Patient> results = db.Query<Patient>(
                    delegate(Patient vs)
                    {
                        return vs.HealthCareUserID == huserID;
                    }
                );

            return results;
        }
        /// <summary>
        /// Will retrieve all events on a patient
        /// </summary>
        /// <param name="patient">the patient to fetch the data from</param>
        /// <returns>A list of patients</returns>
        public IList<Event> RetrieveAllEventsOnPatient(Patient patient)
        {
            //Perform a query - using the delegate approach. All events having this patient ID.
            IList<Event> results = db.Query<Event>(
                delegate(Event vs)
                {
                    return vs.PatientID == patient.ID;
                }
            );

            return results;
        }

        /// <summary>
        /// Will retrieve all notes on a patient
        /// </summary>
        /// <param name="patient"></param>
        /// <returns></returns>
        public IList<Note> RetrieveAllNotesOnPatient(Patient patient)
        {
            //Perform a query - using the delegate approach. All notes having this patient ID.
            IList<Note> results = db.Query<Note>(
                delegate(Note vs)
                {
                    return vs.PatientID == patient.ID;
                }
            );

            return results;
        }

        /// <summary>
        /// This will create a new unique User
        /// </summary>
        /// <param name="obj">A Patient value object</param>
        public UserDTO CreateUser(UserDTO obj)
        {
            HandleID(obj);

            //We have to test for uniqueness - here only defined through a unique social security number
            var user = RetrieveUserBySocialSecurity(obj.SocialSecurity);
            if (user != null)
            {
                //throw new Exception("User already exists on this social security number!");
                return user; 
            }

            db.Store(obj);
            return obj;
        }

        /// <summary>
        /// This will fetch an existing object by its ID (from the DTO obj) and 
        /// use reflection to copy all properties from the original object - regardless of which
        /// subtype of DTO object we are using. Why are we not just using "object" - we need the ID.
        /// </summary>
        /// <param name="obj">the new data transfer object containing the new values for updating</param>
        public void UpdateObject(AbstractDTO obj)
        {
            var storedObj = RetrieveObjectByID(obj.ID);
            
            //Run through all properties and copy the data from the new object to the old before storing it
            foreach (var propInfo in (obj.GetType()).GetProperties()) {
                //Get the the property we want to copy data from and to.
                PropertyInfo copyTo = (storedObj.GetType()).GetProperty(propInfo.Name);
                //set the value on the storedObj (the object that lives in the datastore) 
                copyTo.SetValue(storedObj, propInfo.GetValue(obj, null), null);
            }
            db.Store(storedObj);
        }

        /// <summary>
        /// This will retrieve a DTO object from the datastore using only its ID.
        /// It will return any type of object.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public AbstractDTO RetrieveObjectByID(long ID)
        {
            var dto = new AbstractDTO();
            dto.ID = ID;
            IObjectSet result = db.QueryByExample(dto);
            if (result.Count > 0) return (AbstractDTO)result.Next();
            return null;
        }

        /// <summary>
        /// Retrieves a userDTO by using the social security and passkey constructs
        /// </summary>
        /// <param name="socialSecurity"></param>
        /// <param name="passKey"></param>
        /// <returns></returns>
        public UserDTO RetrieveUserBySocialSecurityAndPassKey(string socialSecurity, string passKey)
        {
            //var user = new UserDTO();
            //user.SocialSecurity = socialSecurity;
            //user.PassKey = passKey;
            //IObjectSet result = db.QueryByExample(user);
            //if (result.Count > 0) return (UserDTO)result.Next();
            //return null;

            //Perform a query - using the delegate approach. All notes having this patient ID.
            IList<UserDTO> found = db.Query<UserDTO>(
                delegate(UserDTO dto)
                {
                    return (dto.SocialSecurity.Equals(socialSecurity) && dto.PassKey.Equals(passKey));
                }
            );
            if (found != null && found.Count() > 0) return found[0];
            else return null;// throw new Exception("User not found with the provided social security and passkey combination.");
        }

        /// <summary>
        /// Retrieve users by socialsecurity string
        /// </summary>
        /// <param name="socialSecurity">The string describing the socialsecurity number</param>
        /// <returns>A user with the socialsecurity number</returns>
        public UserDTO RetrieveUserBySocialSecurity(string socialSecurity)
        {
            var user = new UserDTO();
            user.SocialSecurity = socialSecurity;
            IObjectSet result = db.QueryByExample(user);
            if (result.Count > 0) return (UserDTO)result.Next();
            return null;
        }

        /// <summary>
        /// Retrieve ALL patients
        /// </summary>
        /// <returns></returns>
        public Patient[] RetrieveAllPatients()
        {
            IList<Patient> patients = db.Query<Patient>(typeof(Patient));
            return patients.ToArray<Patient>();
        }

        /// <summary>
        /// Retrieve ALL healthcare users
        /// </summary>
        /// <returns></returns>
        public HealthCareUser[] RetrieveAllHealthcareUsers()
        {
            IList<HealthCareUser> patients = db.Query<HealthCareUser>(typeof(HealthCareUser));
            return patients.ToArray<HealthCareUser>();
        }

        /// <summary>
        /// Assign a sequential ID
        /// </summary>
        /// <returns>An ID that is unique in this database</returns>
        public long AssignID()
        {
            //Intialize helper parameters
            long result = 0;
            ID id;
            
            //Search for IDs
            IList<ID> ids = db.Query<ID>(typeof(ID));
            
            //IF there are no IDs in the database, create one - else reuse the existing field
            if (ids.Count == 0)
            {
                id = new ID();
            } else
                id = ids[0];
            
            //increment
            result = ++id.LastIDAssigned;
            //and store
            db.Store(id);
            return result;
        }

        /// <summary>
        /// Will attach the user object to the healthcare user
        /// </summary>
        /// <param name="p">patient to attaach</param>
        /// <param name="u">healthcare user to attach</param>
        public void AttachPatientToHealthCareUser(Patient p, HealthCareUser u)
        {
            var patientDB = (Patient)RetrieveUserBySocialSecurity(p.SocialSecurity);
            var huserDB = RetrieveUserBySocialSecurity(u.SocialSecurity);

            patientDB.HealthCareUserID = huserDB.ID;

            db.Store(patientDB);

        }

        /// <summary>
        /// Will remove all data that are expired (on the count of their Expired attribute) from the data store.
        /// </summary>
        /// <returns>the number of objects removed</returns>
        public int RemoveExpiredData()
        {
            //Perform a query - using the delegate approach. All notes having this patient ID.
            IList<AbstractDTO> expiredObjects = db.Query<AbstractDTO>(
                delegate(AbstractDTO dto)
                {
                    return (dto.Expires != null && dto.Expires.Ticks < DateTime.Now.Ticks);
                }
            );
            int numberOfObjectsToDelete = expiredObjects.Count();

            foreach (var dto in expiredObjects)
            {
                db.Delete(dto);
            }
            //db.Commit();

            return numberOfObjectsToDelete;
        }

        /// <summary>
        /// This will commit all datastore actions
        /// </summary>
        public void Commit()
        {
            db.Commit();
        }

        /// <summary>
        /// Will reverse all data storage actions who have not yet been commited.
        /// </summary>
        public void RollBack()
        {
            db.Rollback();
        }
    }

    /// <summary>
    /// Used for sequencing the IDs
    /// </summary>
    public class ID
    {
       public long LastIDAssigned { get; set; }

    }
}
