﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;

///This name space cntains all Data Transfer Objects that is used in the required solution
///Pervasive Heatlhcare Evaluation Infrastructure - PHEI
namespace PHEI.DTO
{
    /// <summary>
    /// This is the abstract basic Data Transfer Object
    /// ALl DTO objects must descend from this.
    /// </summary>


    [Serializable]
    public class AbstractDTO
    {

        /// <summary>
        /// The unique ID of this class
        /// </summary>
        public long ID { get; set;  }

        /// <summary>
        /// Decides the date in which this object is allowed to live on the server.
        /// The server has the responsibility to delete objects that are not allowed to live for 
        /// prolonged periods of time on the central server.
        /// If for instance the user will only allow his doctor to view the data for 1 week - the
        /// data must be deleted within this time frame.
        /// </summary>
        public DateTime Expires { get; set; }

    }

    /// <summary>
    /// The Context class is the super class for all context data.
    /// Context types that are not known at design time can be extended from this one,
    /// or this may be used as simple tuple
    /// </summary>
    [Serializable]
    public class Context : AbstractDTO
    {

        /// <summary>
        /// The Time of creation of this observation
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// A short hand description of the context observation
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// A longer description of the vital sign
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// This identifies the equipment associated with the event 
        /// TODO: consider modelling this as a seperate class
        /// </summary>
        public string Equipment { get; set; }

        /// <summary>
        /// The person ID to whom this context belongs
        /// </summary>
        public long PatientID { get; set; }

    }


    /// <summary>
    /// Weight is the weight of the patient measured in grams - and on a given date time
    /// </summary>
    [Serializable]
    public class UserSeatPosition: Context
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public UserSeatPosition()
        {
            ShortDescription = "SeatPosition";
        }

        /// <summary>
        /// is the user seated
        /// </summary>
        public bool Seated { get; set; }

        /// <summary>
        /// is the user seated
        /// </summary>
        public bool BackSupported { get; set; }

        /// <summary>
        /// Are the legs NOT crossed
        /// </summary>
        public bool LegsNotCrossed { get; set; }

    }

    [Serializable]
    public class NoiseLevel : Context
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NoiseLevel()
        {
            ShortDescription = "The  noise level in dB";
        }

        /// <summary>
        /// The current user context noise level in dB
        /// </summary>
        public long NoiseLeveldB { get; set; }

    }


    /// <summary>
    /// The vital sign class is the super class for all vital signs.
    /// Vital Signs that are not known at design time can be extended from this one,
    /// or this may be used as simple tuple
    /// </summary>
    [Serializable]
    [KnownType(typeof(BloodPressure))]
    [KnownType(typeof(Weight))]
    [KnownType(typeof(Oximeter))]
    [KnownType(typeof(ECG))]
    [KnownType(typeof(HeartRate))]
    [KnownType(typeof(BloodSugar))]
    public class VitalSign : AbstractDTO {

        /// <summary>
        /// The Time of creation of this event
        /// </summary>
        public DateTime TimeStamp { get; set; }
        
        /// <summary>
        /// A short hand description of the vital sign
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// A longer description of the vital sign
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// This identifies the equipment associated with the event 
        /// TODO: consider modelling this as a seperate class
        /// </summary>
        public string Equipment { get; set; }

        /// <summary>
        /// The person ID to whom this vital sign belongs
        /// </summary>
        public long PatientID { get; set; }

    }

    /// <summary>
    /// Blood pressure is the most principal vital sign 
    /// </summary>
    [Serializable]
    public class BloodPressure : VitalSign
    {
        /// <summary>
        /// The constructor for a blood pressure
        /// </summary>
        public BloodPressure()
        {
            ShortDescription = "Bloodpressure";
        }

        /// <summary>
        /// Systolic pressure is peak pressure in the arteries, 
        /// which occurs near the end of the cardiac cycle when the ventricles are contracting.
        /// Systolic is the maximum blood pressure measured
        /// Measured in mm Hg
        /// </summary>
        public int Systolic { get; set; }

        /// <summary>
        /// Diastolic pressure is minimum pressure in the arteries, which occurs near the 
        /// beginning of the cardiac cycle when the ventricles are filled with blood.
        /// Measured in mm Hg
        /// </summary>
        public int Diastolic{ get; set; }
        
        /// <summary>
        /// Heart rate is the number of heartbeats  per unit of time 
        /// - in beats per minute (bpm) 
        /// </summary>
        public int HeartRate { get; set; }
    }

    /// <summary>
    /// Weight is the weight of the patient measured in grams - and on a given date time
    /// </summary>
    [Serializable]
    public class Weight : VitalSign
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Weight()
        {
            ShortDescription = "Weight";
        }

        /// <summary>
        /// Will return the weight measured in grams
        /// </summary>
        public int WeigthInGrams { get; set; }


    }

    /// <summary>
    /// HearRate is measured in BPM 
    /// </summary>
    [Serializable]
    public class HeartRate: VitalSign
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public HeartRate()
        {
            ShortDescription = "Heart Rate";
        }

        /// <summary>
        /// Will return the weight measured in grams
        /// </summary>
        public int HeartRateInBPM { get; set; }


    }

    /// <summary>
    /// Blood Sugar
    /// </summary>
    [Serializable]
    public class BloodSugar : VitalSign
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public BloodSugar()
        {
            ShortDescription = "Blood Sugar";
        }

        /// <summary>B
        /// Will return the weight measured in grams
        /// </summary>
        public int BloodSugarLevel { get; set; }


    }



    /// <summary>
    /// A measurement of the heart electro activity
    /// </summary>
    [Serializable]
    public class ECG : VitalSign
    {
        public ECG()
        {
            ShortDescription = "ECG";
        }
        public byte[] ECGValues { get; set; }
    }

    /// <summary>
    /// The oxygen saturation of the blood 
    /// </summary>
    [Serializable]
    public class Oximeter : VitalSign
    {
        public Oximeter()
        {
            ShortDescription = "Oximeter";
        }
        public double SaturationLevel { get; set; }
        public int HeartRate { get; set; }
    }

    /// <summary>
    /// Is used to give an indication of the type of event
    /// </summary>
    public enum EventType { Information, Alert, MajortAlert, CriticalAlert }

    /// <summary>
    /// Event is something that has happend - it could be a new measurement - it could be an alert - e.g. too high blood pressure
    /// or that a device has failed to work ...
    /// </summary>
    [Serializable]
    public class Event : AbstractDTO
    {

        /// <summary>
        /// The Time of creation of this event
        /// </summary>
        public DateTime TimeStamp { get; set; }
        
        /// <summary>
        /// The event type decides on the complexity of the object
        /// </summary>
        public EventType EventType { get; set; }
        
        /// <summary>
        /// The subject is a small textual definition of the subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// The main text contains a closer event description
        /// </summary>
        public string MainText { get; set; }

        /// <summary>
        /// This identifies the equipment associated with the event 
        /// TODO: consider modelling equ
        /// </summary>
        public string Equipment{ get; set; }

        /// <summary>
        /// In case this event occurs in relation to a vital sign, the vital sign may be added.
        /// </summary>
        public long RelatedVitalSignID { get; set; }

        /// <summary>
        /// The Patient ID to whom this patient belongs
        /// </summary>
        public long PatientID { get; set; }

    }

    /// <summary>
    /// A Note is used for communicating messages, comments and reactions.
    /// </summary>
    [Serializable]
    public class Note : AbstractDTO
    {
      
        /// <summary>
        /// The subject is a small textual definition of the subject of the note
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// The main text contains a closer event description
        /// </summary>
        public string MainText { get; set; }

        /// <summary>
        /// In case this note is made as a comment to a vital sign, the vital sign may be added.
        /// </summary>
        public long RelatedVitalSignID { get; set; }

        /// <summary>
        /// In case this note is made as a comment to an event event may be added.
        /// </summary>
        public long RelatedEventID { get; set; }

        /// <summary>
        /// In case this note is made as a comment to another note this.
        /// </summary>
        public long RelatedNoteID{ get; set; }

        /// <summary>
        /// If this note belongs to a patient ID
        /// </summary>
        public long PatientID { get; set; }

    }

    /// <summary>
    /// The user object is a supertype for all user types
    /// </summary>
    [Serializable]
    [KnownType(typeof(HealthCareUser))]
    [KnownType(typeof(Patient))]
    public class UserDTO : AbstractDTO
    {
        /// <summary>
        /// Timestamp for creation of this patient
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Name of the patient
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The unique identifier
        /// </summary>
        public string SocialSecurity { get; set; }

        /// <summary>
        /// Determines whether the user is part of the control group or not
        /// </summary>
        public bool IsControlGroup { get; set; }

        /// <summary>
        /// Determines whether the will recieve calm guidance
        /// </summary>
        public bool CalmGuidance { get; set; }

        /// <summary>
        /// Determines
        /// </summary> whether the user will recieve classic guidance
        public bool ClassicGuidance { get; set; }

        /// <summary>
        /// The passkey that is used to give acess to this user to report data
        /// </summary>
        public string PassKey { get; set; }

        /// <summary>
        /// The email is used to report data
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone number is used for contacting
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// SIP adress number is used for contacting
        /// </summary>
        public string SIP { get; set; }

        /// <summary>
        /// SIPPassCode for contacting
        /// </summary>
        public string SIPPassCode { get; set; }

    }

    /// <summary>
    /// Models the different data distribution strategies that this patient will prefer.
    /// </summary>
    public enum DistributionStrategy { Email, OpenCareVault, GoogleHealth, MicrosoftHealthVault };
 
    /// <summary>
    /// The patient to whom the vital signs belongs
    /// </summary>
    [Serializable]
    public class Patient : UserDTO
    {

        /// <summary>
        /// Constructor
        /// </summary>
        public Patient()
        {
           
            # region strongDTO
            //VitalSigns = new List<VitalSign>();
            //Events = new List<Event>();
            # endregion strongDTO
        }

        /// <summary>
        /// The Related healthcare user
        /// </summary>
        public long HealthCareUserID{ get; set; }

        //TODO: Consider refactoing this into a strategy pattern
        /// <summary>
        /// A list of distribution strategies selected by this patient
        /// </summary>
        public DistributionStrategy[] DistributionStrategies {get; set; }

        # region strongDTO

        ///// <summary>
        ///// A list of vital signs reported on this patient
        ///// </summary>
        //public List<VitalSign> VitalSigns { get; set; }
        ///// <summary>
        ///// A list of events reported on this patient
        ///// </summary>
        //public List<Event> Events{ get; set; }
        
        # endregion strongDTO
    }


    /// <summary>
    /// A healthcare user and all his patients
    /// </summary>
    [Serializable]
    public class HealthCareUser : UserDTO
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public HealthCareUser()
        {

            # region strongDTO
            
            //Patients = new List<Patient>();
            
            # endregion strongDTO

            IsAdmin = true;

        }

        /// <summary>
        /// The email is used to report data
        /// TODO: Consider refactoring this
        /// </summary>
        public bool IsAdmin { get; set; }

        # region strongDTO

        ///// <summary>
        ///// A list of vital signs reported on this patient
        ///// </summary>
        //public List<Patient> Patients { get; set; }

        # endregion strongDTO

    }
}
