﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PHEI.DTO;
using VitalSignsDAL;

///This namespace contains all busines logic for the solution. It must not depend on 
///any presentation layers, e.g. UI or Communication, but solely on this solution.
namespace PHEI.BLL
{
    /// <summary>
    /// This is a prototype class displaying a patientcontroller
    /// </summary>
    public class PatientController
    {
        private string fileName = "VitalSigns.db";
        private DAL Database = null;        
        private Patient CurrentUser {get; set; }

        /// <summary>
        /// Constructor for this controller. 
        /// </summary>
        /// <param name="fileName">path to the the database</param>
        public PatientController(string fileName) {

            if (fileName != null)
                this.fileName = fileName;

            Database = DAL.Instance(fileName);
        }

        ~PatientController()
        {
            try
            {
                //Database.CloseDatabase();
            }
            catch { }
        }


        /// <summary>
        /// Will create a vital sign on a patient with this socialsecurity number and passkey
        /// TODO: consider synchronizing the code
        /// </summary>
        /// <param name="vitalSign">the DTO object to add to the patients record</param>
        /// <param name="socialSecurity">the patients social security number as a string</param>
        /// <param name="passKey">the patiens passkey as created originally</param>
        public void AddVitalSign(VitalSign vitalSign, string socialSecurity, string passKey)
        {

            try
            {   
                //Check that the patient exists and create a DTO for him if he does
                var patient = Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passKey);
                if (patient != null && patient is Patient) {
                    vitalSign.PatientID = patient.ID;
                    Database.CreateDTO(vitalSign);
                }
                //Commit the changes
                Database.Commit();
            }
            catch
            {
                //If something went wrong - do the rollback operation
                Database.RollBack();
                //Throw an exception
                throw new Exception("Vital Sign Could not be created, check social security number and passkey");
            }
        }


        /// <summary>
        /// Will create a collectin of vital signs on a patient with this socialsecurity number and passkey
        /// while also setting a common expiry date on the data. This means, that data will be removed from 
        /// the server after this expiry date.
        /// TODO: consider synchronizing the code        
        /// </summary>
        /// <param name="vitalSigns">An array of DTO object to add to the patients record</param>
        /// <param name="Expires">The Date of expiry of the data</param>
        /// <param name="socialSecurity">the patients social security number as a string</param>
        /// <param name="passKey">the patiens passkey as created originally</param>
        public void AddVitalSigns(VitalSign[] vitalSigns, DateTime Expires, string socialSecurity, string passKey)
        {
            try
            {
                //Check that the patient exists and create a DTO for him if he actually does exist
                var patient = Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passKey);
                if (patient != null && patient is Patient)
                {
                    foreach (var vitalSign in vitalSigns)
                    {
                        vitalSign.PatientID = patient.ID;
                        vitalSign.Expires = Expires;
                        Database.CreateDTO(vitalSign);
                    }
                }
                //Commit the changes
                Database.Commit();
            }
            catch
            {
                //If something went wrong - do the rollback operation
                Database.RollBack();
                //Throw an exception
                throw new Exception("Vital Sign Could not be created, check social security number and passkey");
            }
        }

        /// <summary>
        /// Will create an event on a patient with this socialsecurity number and passkey
        /// TODO: consider synchronizing the code
        /// </summary>
        /// <param name="ev">A DTO object containing the event to store</param>
        /// <param name="socialSecurity">the patients social security number as a string</param>
        /// <param name="passKey">the patiens passkey as created originally</param>
        public void AddEvent(Event ev, string socialSecurity, string passKey)
        {

            try
            {
                //Check that the patient exists and create a DTO for him if he does
                var patient = Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passKey);
                if (patient != null && patient is Patient)
                {
                    ev.PatientID = patient.ID;
                    Database.CreateDTO(ev);
                    Database.Commit(); //Commit the changes
                }
            }
            catch (Exception e)
            {
                //Rollback the transactions in case of anything went wrong
                Database.RollBack();
                throw new Exception("Event Could not be created, check social security number and passkey: "+e.Message);
            }
        }

        /// <summary>
        /// Will create a collection of events on a patient with this socialsecurity number and passkey
        /// while also setting a common expiry date on the data. This means, that data will be removed from 
        /// the server after this expiry date.
        /// TODO: consider synchronizing the code        
        /// </summary>
        /// <param name="events">An array of DTO object to add to the patients record</param>
        /// <param name="Expires">The Date of expiry of the data</param>
        /// <param name="socialSecurity">the patients social security number as a string</param>
        /// <param name="passKey">the patiens passkey as created originally</param>        
        public void AddEvents(Event[] events, DateTime Expires, string socialSecurity, string passKey)
        {
            try
            {
                //Check that the patient exists and create a DTO for him if he does
                var patient = Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passKey);
                if (patient != null && patient is Patient)
                {
                    foreach (var ev in events)
                    {
                        ev.PatientID = patient.ID;
                        ev.Expires = Expires;
                        Database.CreateDTO(ev);
                    }
                    Database.Commit(); //Commit the changes
                }
            }
            catch
            {
                //Rollback the transactions in case of anything went wrong
                Database.RollBack();
                throw new Exception("Event Could not be created, check social security number and passkey");
            }
        }
        /// <summary>
        /// Gets all events on a patient with the given social security and passkey
        /// </summary>
        /// <param name="socialSecurity">the string containing the social security number of the patient</param>
        /// <param name="passkey">the passcode of the user</param>
        /// <returns>a simple array of events</returns>
        public Event[] RetrieveAllEvents(string socialSecurity, string passkey)
        {
            var user = Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passkey);
            if (user is Patient)
                return Database.RetrieveAllEventsOnPatient((Patient)user).ToArray();
            return null;

        }

        /// <summary>
        /// Gets all vital signs on a patient with the given social security and passkey
        /// </summary>
        /// <param name="socialSecurity">the string containing the social security number of the patient</param>
        /// <param name="passkey">the passcode of the user</param>
        /// <returns>a simple array of events</returns>
        public VitalSign[] RetrieveAllVitalSigns(string socialSecurity, string passkey)
        {
            var user = Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passkey);
            if (user is Patient)
                 return Database.RetrieveAllVitalSignsOnPatient((Patient)user).ToArray();
            return null;

        }

        /// <summary>
        /// A patient may only update his own information - and may not change (NOTE: changed - may now change socialsecurity) and 
        /// ID of the patient.
        /// </summary>
        /// <param name="patientUpdateInfo">The DTO object holding the new data. Must hae same ID and socialsecurity number.</param>
        /// <returns></returns>
        public void UpdatePatient(Patient patientUpdateInfo, string socialSecurity, string passkey) {
            //
            var user = Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passkey);
            if (user.ID == patientUpdateInfo.ID) //&& user.SocialSecurity == patientUpdateInfo.SocialSecurity
            {
                Database.UpdateObject(patientUpdateInfo);
                //Commit the changes
                Database.Commit();
            }
            else
                throw new Exception("Update failed - use correct passkey and social security number - and only update your own patient.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public Patient CreateNewPatient(Patient dto)
        {
            try
            {
                Patient patient = (Patient)Database.CreateUser(dto);
                return patient;
            }
            catch
            {
                return null;
            }
        }


    }
}
