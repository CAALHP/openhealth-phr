﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PHEI.DTO;
using VitalSignsDAL;

///This namespace contains all busines logic for the solution. It must not depend on 
///any presentation layers, e.g. UI or Communication, but solely on this solution.
namespace PHEI.BLL
{
    /// <summary>
    /// This is a business logic layer class modelling a ServerController
    /// The ServerController is  used for maintainance purpsoses and other 
    /// use cases who are system initated - and does not relate to other system actors
    /// such as patients and healthcare personel.
    /// </summary>
    public class ServerController
    {
        private string fileName = "VitalSigns.db";
        private DAL Database = null;
        private Patient CurrentUser { get; set; }

        /// <summary>
        /// Constructor for this controller. 
        /// </summary>
        /// <param name="fileName">path to the the database</param>
        public ServerController(string fileName)
        {

            if (fileName != null)
                this.fileName = fileName;

            Database = DAL.Instance(fileName);
        }

        ~ServerController()
        {
            try
            {
                //Database.CloseDatabase();
            }
            catch { }
        }

        /// <summary>
        /// Will remove all data that are expired (on the count of their Expired attribute) from the data store.
        /// </summary>
        /// <returns>the number of objects removed</returns>
        public int RemoveExpiredData()
        {
            return Database.RemoveExpiredData();
        }
    }
    }