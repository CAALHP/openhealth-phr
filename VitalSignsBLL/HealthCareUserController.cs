﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PHEI.DTO;
using VitalSignsDAL;


namespace PHEI.BLL
{
    public class HealthCareUserController
    {
        private string fileName = "VitalSigns.db";
        private DAL Database = null;        
        private HealthCareUser CurrentUser {get; set; }

        /// <summary>
        /// Constructor for this controller. 
        /// </summary>
        /// <param name="fileName">path to the the database</param>
        public HealthCareUserController(string fileName) {

            if (fileName != null)
                this.fileName = fileName;

            Database = DAL.Instance(fileName);
        }

        /// <summary>
        /// Destructor will perform the clean up
        /// </summary>
        ~HealthCareUserController()
        {
            try
            {
                //Database.CloseDatabase();
            }
            catch { }
        }

        /// <summary>
        /// Create a new patient - and connects that patient the current healthcare user
        /// </summary>
        /// <param name="dto">DTO data of the patient</param>
        /// <param name="socialSecurity">SocialSeurity of the Healthcare user</param>
        /// <param name="passkey">passkey of the healthcare user</param>
        /// <returns></returns>
        public Patient CreateNewPatient(Patient dto, string socialSecurity, string passkey)
        {
            var user = (HealthCareUser)Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passkey);

            if (user != null && user.ID > 0)
            {
                dto.HealthCareUserID = user.ID;
                Database.CreateUser(dto);
                return dto;
            }
            else
                throw new Exception("User could not be found");
        }


        /// <summary>
        /// This will retrieve the complete list of patients belonging to the current healthcare user - and all related data as
        /// a graph. Create your own functionality to complete the remaining requirements.
        /// This requires the user to be an admin user
        /// </summary>
        /// <returns>A list of patients</returns>
        public Patient[] RetrieveAllPatients(string socialSecurity, string passkey)
        {
            try
            {
                var user = (HealthCareUser)Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passkey);
                //if (user.IsAdmin)
                    return Database.RetrieveAllPatientsOnHealthcareUser(user).ToArray();
            }
            catch
            {
            }
            return null;

        }

        /// <summary>
        /// This will retrieve the complete list of patients that are NOT - assigned to a healthcare professional.
        /// This requires the user to be an admin user
        /// </summary>
        /// <returns>A list of patients</returns>
        public Patient[] RetrieveAllUnassignedPatients()
        {
            try
            {
                return Database.RetrieveAllPatients().ToArray();
            }
            catch
            {
            }
            return null;

        }

        /// <summary>
        /// This will retrieve the complete list of patients that are NOT - assigned to a healthcare professional.
        /// This requires the user to be an admin user
        /// </summary>
        /// <returns>A list of patients</returns>
        public HealthCareUser[] RetrieveAllHealthcareUsers()
        {
            try
            {
                return Database.RetrieveAllHealthcareUsers().ToArray();
            }
            catch
            {
            }
            return null;

        }
        /// <summary>
        /// This will retrieve the complete list of patients belonging to the current healthcare user - and all related data as
        /// a graph. Create your own functionality to complete the remaining requirements.
        /// </summary>
        /// <returns>A list of patients</returns>
        public Patient[] RetrieveAllPatients(int HealthCareUserID)
        {
            try
            {
                //var user = (HealthCareUser)Database.RetrieveUserBySocialSecurityAndPassKey(socialSecurity, passkey);
                return Database.RetrieveAllPatientsOnHealthcareUser(HealthCareUserID).ToArray();
            }
            catch
            {
            }
            return null;

        }

        /// <summary>
        /// Will create a new healthcare user
        /// </summary>
        /// <param name="dto">the data of the healthcare user to create</param>
        /// <returns>the newly created user with an ID</returns>
        public HealthCareUser CreateNewHealthCareUser(HealthCareUser dto)
        {
            Database.CreateUser(dto);
            return dto;
        }


    }
}
