﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PHEI.DTO;
using VitalSignsDAL;
using PHEI.BLL;
using System.Threading;

namespace PHEI.TestConsoleApplication
{
    /// <summary>
    /// This is a console application to demo the workings of the DAL layer
    /// The data used in here should instead be incorporated into a DAL layer.
    /// </summary>
    class Program
    {
        private static string fileName = "VitalSigns.db";

        static void Main(string[] args) {

            var Database = DAL.Instance(fileName);

            try
            {

                //Get input from the user to create a new Patient
                Console.WriteLine("Enter new Patient Name: ");
                string patientName = Console.ReadLine();
                Console.WriteLine("Enter new Patient Social Security: ");
                string socialSecurity = Console.ReadLine();
                Console.WriteLine("Enter desired passkey: ");
                string passkey = Console.ReadLine();


                //Create the new patient object
                var patient = new Patient();
                patient.Name = patientName;
                patient.SocialSecurity = socialSecurity;
                patient.PassKey = passkey;
                patient.TimeStamp = DateTime.Now;
                
                try
                {
                    //Store the patient and the related data
                    Database.CreateUser(patient);
                    Database.Commit();
                }
                catch
                {
                    Console.WriteLine("Patient with socialsecurity " + patient.SocialSecurity + " already exists!!!");
                }

                //Create a demo blood pressure reading
                var bp = new BloodPressure();
                bp.Systolic = 130;
                bp.Diastolic = 85;
                bp.HeartRate = 60;
                bp.TimeStamp = DateTime.Now;
                bp.PatientID = patient.ID;
                Database.CreateDTO(bp);
                Database.Commit();

                //The following is an update of the original blood pressure - where we do NOT have a strong reference
                var bp3 = new BloodPressure();
                bp3.ID = bp.ID;
                bp3.Systolic = 145;
                bp3.Diastolic = 90;
                bp3.HeartRate = 62;
                bp3.TimeStamp = bp.TimeStamp;
                Database.UpdateObject(bp3);
                Database.Commit();

                //Add the blood pressure measurement to the patient
              //  patient.VitalSigns.Add(bp);

                //Create a second demo blood pressure reading
                var bp2 = new BloodPressure();
                bp2.Systolic = 135;
                bp2.Diastolic = 90;
                bp2.HeartRate = 65;
                bp2.TimeStamp = DateTime.Now;
                bp2.PatientID = patient.ID;
                Database.CreateDTO(bp2);
                Database.Commit();

                //Add the blood pressure measurement to the patient
                //patient.VitalSigns.Add(bp2);


                var doctor = new HealthCareUser();
                doctor.IsAdmin = false;
                doctor.Name = "Doktor Jens Hansen";
                doctor.SocialSecurity = patient.SocialSecurity + "00";
                doctor.PassKey = patient.PassKey + "00";
                doctor.Email = "sw@iha.dk";
                Database.CreateUser(doctor);
                Database.Commit();

                Database.AttachPatientToHealthCareUser(patient, doctor);
                Database.Commit();

                //Fetch all patients from the database
                Patient[] patiens = Database.RetrieveAllPatients();

                //Repeat for all patients to output the data on them to the UI
                foreach (var p in patiens)
                {
                    Console.WriteLine("Found: " + p.ID + " " + p.Name + " - " + p.SocialSecurity + " Created at: " + p.TimeStamp);

                    //Create a third demo blood pressure reading
                    bp = new BloodPressure();
                    bp.Systolic = 140;
                    bp.Diastolic = 95;
                    bp.HeartRate = 65;
                    bp.TimeStamp = DateTime.Now;
                    bp.PatientID = patient.ID;

                    Database.CreateDTO(bp);

  

                    //Create an event
                    var ev = new Event();
                    ev.TimeStamp = DateTime.Now;
                    ev.Subject = "Test Event";
                    ev.MainText = "This is a test event";
                    //ev.RelatedVitalSign = bp;

                    ev.RelatedVitalSignID = bp.ID;
                    ev.PatientID = patient.ID;

                    Database.CreateDTO(ev);

                     foreach (var vs in Database.RetrieveAllVitalSignsOnPatient(p)) {
                     if (vs is BloodPressure)
                        Console.WriteLine("Found a: " + ((BloodPressure)vs).ShortDescription + " from: "+((BloodPressure)vs).TimeStamp.ToShortTimeString() + ": " + ((BloodPressure)vs).Systolic + "/" + ((BloodPressure)vs).Diastolic);
                     }

                     //Are there any events on the patient?
                     foreach (var vs in Database.RetrieveAllEventsOnPatient(patient))
                     {
                         //
                         if (vs is Event)
                             Console.WriteLine("Found event ID: " + ((Event)vs).ID + " from: " + ((Event)vs).TimeStamp.ToShortTimeString() + ": " + ((Event)vs).Subject + "/" + ((Event)vs).MainText);
                     }
                }

                Database.Commit();

                //Wait for hte user to end
                Console.WriteLine("Press any key to end");
                Console.ReadLine();


            Console.WriteLine("And now let's get the user through the controller");
            Console.WriteLine("Enter Patient Social Security: ");
            string socialSecurity2 = Console.ReadLine();
            Console.WriteLine("Enter desired passkey: ");
            string passkey2 = Console.ReadLine();
            
            //And now - through the PatientController layer instead!
            var controller = new PatientController(fileName);
            var eve = new Event();
            eve.TimeStamp = DateTime.Now;
            eve.Subject = "Fall detected!!";
            eve.EventType = EventType.MajortAlert;
            eve.MainText = "Please go and check patient home";

            controller.AddEvent(eve, socialSecurity2, passkey2);

            Event[] events = controller.RetrieveAllEvents(socialSecurity2, passkey2);
            foreach (var vs in events) 
            {
                //
                if (vs is Event)
                    Console.WriteLine("Found event ID: " + ((Event)vs).ID + " from: " + ((Event)vs).TimeStamp.ToShortTimeString() + ": " + ((Event)vs).Subject + "/" + ((Event)vs).MainText);
            }

            //Wait for hte user to end
            Console.WriteLine("Press any key to end");
            Console.ReadLine();

            }
            finally
            {
                //Close the database for existing
                Database.CloseDatabase();
            }

        }
    }
}
