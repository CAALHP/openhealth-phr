﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ServiceModel.Description;
using PHEI.BLL;
using PHEI.DTO;
using caalhp.Core.Contracts;
using caalhp.IcePluginAdapters;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;

namespace OpenHealthPHRService
{
    [ServiceContract]
    public interface IPatientService
    {

        [OperationContract]
        void AddVitalSign(VitalSign vitalSign, string socialSecurity, string passKey);
        [OperationContract]
        void AddVitalSigns(VitalSign[] vitalSigns, DateTime Expires, string socialSecurity, string passKey);
        [OperationContract]
        void AddEvent(Event ev, string socialSecurity, string passKey);
        [OperationContract]
        void AddEvents(Event[] events, DateTime Expires, string socialSecurity, string passKey);
        [OperationContract]
        Event[] RetrieveAllEvents(string socialSecurity, string passkey);
        [OperationContract]
        VitalSign[] RetrieveAllVitalSigns(string socialSecurity, string passkey);
        [OperationContract]
        void UpdatePatient(Patient patientUpdateInfo, string socialSecurity, string passkey);
        [OperationContract]
        Patient CreateNewPatient(Patient dto);
        //[OperationContract]
        //Patient[] RetrieveAllPatients(string socialSecurity, string passkey);
        [OperationContract]
        void Test(BloodPressure bp);
        [OperationContract]
        Patient CreateNewPatientOnHealthCareUser(Patient dto, string socialSecurity, string passkey);
        [OperationContract]
        Patient[] RetrieveAllPatientsOnHealthCareProfessional(string socialSecurity, string passkey);
        [OperationContract]
        Patient[] RetrieveAllUnassignedPatients();
        [OperationContract]
        Patient[] RetrieveAllPatients(int HealthCareUserID);
        [OperationContract]
        HealthCareUser[] RetrieveAllHealthcareUsers();
        [OperationContract]
        HealthCareUser CreateNewHealthCareUser(HealthCareUser dto);
    }

    /// <summary>
    /// 
    /// </summary>
    public class PatientService : IPatientService
    {

        private string DB_NAME = "vitalsigns.db";

        public void AddVitalSign(VitalSign vitalSign, string socialSecurity, string passKey)
        {
            var pc = new PatientController(DB_NAME);
            pc.AddVitalSign(vitalSign, socialSecurity, passKey);
            
        }

        public void AddVitalSigns(VitalSign[] vitalSigns, DateTime Expires, string socialSecurity, string passKey)
        {
            var pc = new PatientController(DB_NAME);
            pc.AddVitalSigns(vitalSigns, Expires, socialSecurity, passKey);
        }

        public void AddEvent(PHEI.DTO.Event ev, string socialSecurity, string passKey)
        {
            var pc = new PatientController(DB_NAME);
            pc.AddEvent(ev, socialSecurity, passKey);
        }

        public void AddEvents(PHEI.DTO.Event[] events, DateTime Expires, string socialSecurity, string passKey)
        {
            var pc = new PatientController(DB_NAME);
            pc.AddEvents(events, Expires, socialSecurity, passKey);
        }

        public PHEI.DTO.Event[] RetrieveAllEvents(string socialSecurity, string passkey)
        {
            var pc = new PatientController(DB_NAME);
            return pc.RetrieveAllEvents(socialSecurity, passkey);
        }

        public VitalSign[] RetrieveAllVitalSigns(string socialSecurity, string passkey)
        {
            var pc = new PatientController(DB_NAME);
            return pc.RetrieveAllVitalSigns(socialSecurity, passkey);
        }

        public void UpdatePatient(Patient patientUpdateInfo, string socialSecurity, string passkey)
        {
            var pc = new PatientController(DB_NAME);
            pc.UpdatePatient(patientUpdateInfo, socialSecurity, passkey);
        }


        public Patient CreateNewPatient(Patient dto)
        {
            var pc = new PatientController(DB_NAME);
            return pc.CreateNewPatient(dto);
        }



        public void Test(BloodPressure bp)
        {
            throw new NotImplementedException();
        }


        public Patient CreateNewPatientOnHealthCareUser(Patient dto, string socialSecurity, string passkey)
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.CreateNewPatient(dto, socialSecurity, passkey);
        }

        public Patient[] RetrieveAllPatientsOnHealthCareProfessional(string socialSecurity, string passkey)
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.RetrieveAllPatients(socialSecurity, passkey);
        }

        public Patient[] RetrieveAllUnassignedPatients()
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.RetrieveAllUnassignedPatients();
        }

        public Patient[] RetrieveAllPatients(int HealthCareUserID)
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.RetrieveAllPatients(HealthCareUserID);
        }

        public HealthCareUser[] RetrieveAllHealthcareUsers()
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.RetrieveAllHealthcareUsers();
        }


        public HealthCareUser CreateNewHealthCareUser(HealthCareUser dto)
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.CreateNewHealthCareUser(dto);
        }


    }

    public class OpenHealthHostingService
    {
        private ServiceHost host;
        /// <summary>
        /// The base adress where to reach this service (refactor to settings file)
        /// </summary>
        Uri baseAddress = new Uri("http://localhost:8083/VitalSignsService");

        /// <summary>
        /// Will start a new service host listing at the given baseadress
        /// </summary>
        public void DoHost()
        {
            try
            {



                // Create the ServiceHost.
                host = new ServiceHost(typeof(PatientService), baseAddress);
                //using (ServiceHost host = new ServiceHost(typeof(PatientService), baseAddress))
                {
                    // Enable metadata publishing.
                    ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                    smb.HttpGetEnabled = true;

                    smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                    host.Description.Behaviors.Add(smb);
                    //host.AddServiceEndpoint(typeof(PatientService), binding, baseAddress.ToString());

                    // Open the ServiceHost to start listening for messages. Since
                    // no endpoints are explicitly configured, the runtime will create
                    // one endpoint per base address for each service contract implemented
                    // by the service.
                    host.Open();

                    Console.WriteLine("The service is ready at {0}", baseAddress);
                    Console.WriteLine("Press <Enter> to stop the service.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred while starting the OEPHR service " + e);
            }
        }

        public void Close()
        {
            if (host != null)
                host.Close();
        }


    }
    /// <summary>
    /// The main service as a Console host app
    /// </summary>
    public class ConsoleHostingServiceImplementation : IServiceCAALHPContract
    {
        private static ServiceAdapter _adapter;
        private static IServiceCAALHPContract _implementation;
        private static OpenHealthHostingService p = null;
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private Patient CurrentPatient;
        private PatientService service;
        private string _cpr;
        private string _passkey;
        private BloodPressure _bpMeasurementPending;


        /// <summary>
        /// Main function (starting point of application - if run from here)
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
                ConsoleHostingServiceImplementation c = new ConsoleHostingServiceImplementation();
                Task t = null;

                Console.WriteLine("Starting OpenHealth Personal Health Record Service");
            try
                {

                    t = Task.Run(() =>
                    {
                        p = new OpenHealthHostingService();
                        p.DoHost();

                    });
                    t.Wait();
            }
            catch { }
            //t.Start();
            //Allows us to do other things ... such as 
            const string endpoint = "localhost";
            try
            {
                _implementation = (IServiceCAALHPContract)(c);
                _adapter = new ServiceAdapter(endpoint, _implementation);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                Console.WriteLine("Failed to Connect to OpenCare: "+e.Message);
            }

            if (t!= null)
                t.Wait();
            //Console.ReadLine();

            // Close the ServiceHost.

            Console.WriteLine("Press <ENTER> to exit program.");
            Console.ReadLine();
            p.Close();
        
        }


        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(caalhp.Core.Events.BloodPressureMeasurementEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(caalhp.Core.Events.SimpleMeasurementEvent)), _processId);

        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public string GetName()
        {
            return "OpenHealthPHR Service";
        }

        public bool IsAlive()
        {
            return true;
        }

        
        /// <summary>
        /// Handles recieving a blood pressure measurement
        /// </summary>
        /// <param name="bpMeasurement"></param>
        private void HandleEvent(caalhp.Core.Events.BloodPressureMeasurementEvent bpMeasurement)
        {
            var vs = new BloodPressure();
            vs.Systolic = (int)bpMeasurement.Systolic;
            vs.Diastolic = (int)bpMeasurement.Diastolic;
            vs.HeartRate = 0; //TODO: update the event

            vs.TimeStamp = bpMeasurement.Timestamp;

            _bpMeasurementPending = vs;
        }

        /// <summary>
        /// Handles recieving all other events
        /// </summary>
        /// <param name="simpleMeasurement"></param>
        private void HandleEvent(caalhp.Core.Events.SimpleMeasurementEvent simpleMeasurement)
        {
            Console.WriteLine("Event recieved: " + simpleMeasurement.MeasurementType);

            try
            {
                service = new PatientService();

                HandlePatientData();

                switch (simpleMeasurement.MeasurementType)
                {
                    case "Oximeter":
                        {
                            //_oximeterManager.AddMeasurement(simpleMeasurement.Value);
                            var vs = new Oximeter();
                            vs.SaturationLevel = simpleMeasurement.Value;
                            vs.TimeStamp = DateTime.Now;
                            service.AddVitalSign(vs, CurrentPatient.SocialSecurity, CurrentPatient.PassKey);
                        } break;
                    case "HeartRate":
                        {
                            var vs = new HeartRate();
                            vs.HeartRateInBPM = (int)simpleMeasurement.Value;
                            vs.TimeStamp = simpleMeasurement.Timestamp;
                            service.AddVitalSign(vs, CurrentPatient.SocialSecurity, CurrentPatient.PassKey);
                            if (_bpMeasurementPending != null)
                            {
                                _bpMeasurementPending.HeartRate = vs.HeartRateInBPM;
                                service.AddVitalSign(_bpMeasurementPending, _cpr, _passkey);
                                _bpMeasurementPending = null;
                            }
                        } break;
                    case "Weight":
                        {
                            //_weightManager.AddMeasurement(simpleMeasurement.Value, simpleMeasurement.UserId);
                            var vs = new Weight();
                            vs.WeigthInGrams = (int)(simpleMeasurement.Value * 1000);
                            vs.TimeStamp = simpleMeasurement.Timestamp;
                            service.AddVitalSign(vs, CurrentPatient.SocialSecurity, CurrentPatient.PassKey);
                        } break;
                    case "BloodSugar":
                        {
                            var vs = new HeartRate();
                            vs.HeartRateInBPM = (int)simpleMeasurement.Value;
                            vs.TimeStamp = simpleMeasurement.Timestamp;
                            service.AddVitalSign(vs, CurrentPatient.SocialSecurity, CurrentPatient.PassKey);
                        } break;
                }
            }
            catch (Exception e) 
            {
                Console.WriteLine("Error during SimpleMeasurement: " + e);
            }
        }

        private void HandlePatientData()
        {
            if (service == null) return;

            try
            {
                Patient[] patients = service.RetrieveAllUnassignedPatients();
                //Patient[] patients = imp.RetrieveAllPatients();

                if (patients == null || patients.Length == 0)
                {
                    CurrentPatient = null;
                    var dto = new Patient();
                    dto.HealthCareUserID = 0;
                    dto.SocialSecurity = "Test";
                    dto.PassKey = "Test";

                    CurrentPatient = service.CreateNewPatient(dto);

                }
                else
                {
                    CurrentPatient = patients[0];

                }
                _cpr = CurrentPatient.SocialSecurity;
                _passkey = CurrentPatient.PassKey;
                //var result = service.RetrieveAllVitalSigns(_cpr, _passkey);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error during handling data: " + e);
            }
        }
        public void Notify(KeyValuePair<string, string> notification)
        {
            try
            {
                //var serializer = EventHelper.GetSerializationTypeFromFullyQualifiedNameSpace(notification.Key);
                var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
                dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
                HandleEvent(obj);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error during Notify: " + e);
            }
        }
        public void ShutDown()
        {
            Environment.Exit(0);
        }
    }
}
